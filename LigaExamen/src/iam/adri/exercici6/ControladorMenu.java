package iam.adri.exercici6;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


public class ControladorMenu implements ControladorInterface, Serializable {
	
	Menu menu;
	Lliga lligaCat;
	public ControladorMenu() throws ExeptionNumeroInsuficientDeJugadors, IOException {
		
		File path1 = new File("./LligaCat");
		 lligaCat = new Lliga(path1);
		 menu = new Menu(lligaCat);
		
	}
	
	public void exportarLliga() throws FileNotFoundException, IOException, ExeptionNumeroInsuficientDeJugadors {

		int opcio;
		do {
			 opcio = menu.menuExporta();
			 
		switch (opcio) {
		case 1:
			cargaSerializable();
			break;
		case 2:
			cargaJson();
			break;
		case 3:
			iniciarMenu();
			
			break;
		default:
			break;
		}}while(opcio!=3);
		
	}
	
	@Override
	public void iniciarMenu() throws IOException, ExeptionNumeroInsuficientDeJugadors {
		
		int opcio;
		do {
			 opcio = menu.menuPrincipal();
		switch (opcio) {
		case 1:
			jugarLliga();
			break;
		case 2:
			gestionarLliga();
			break;
		case 3:
			exportarLliga();
			break;
		case 4:
			
			break;
		default:
			break;
		}}while(opcio!=4);
		
	}

	@Override
	public void jugarLliga() throws IOException {
		lligaCat.jugaLliga();
		menu.mostraMissatge("La lliga s'ha jugat, mira el seu resultat en el historic de lliga!\n");
	}

	@Override
	public void gestionarLliga() throws ExeptionNumeroInsuficientDeJugadors, IOException {
		
	int opcio;
		
		do {
			 opcio = menu.menuGestionarLliga();
		switch (opcio) {
		case 1:
			crearEquip();
			break;

		case 2:
			eliminarEquip();
			break;
		
		case 3:
			modificarEquip();
			break;
		
		case 4:
			enrere();
			break;
			
		case 5:
			guardar();
			break;
		default:
			break;
		}}while(opcio!=4 && opcio!=5);

	}

	@Override
	public void modificarEquip() throws ExeptionNumeroInsuficientDeJugadors, IOException {
		int opcio = menu.menuModificarEquip();
		Equip equipEscollit = null;
		if(opcio!=4 && opcio!=3) {
		equipEscollit = menu.escollirEquip(lligaCat);
		}
		
		switch(opcio) {
		case 1:
			
			menu.eliminarJugador(equipEscollit);
			break;
		case 2:
			
			afegeixJugador(equipEscollit);
			break;
		
		case 3:
			
			canviaNomEquip();
			break;
		
		case 4:
			gestionarLliga();
			break;
			
		default:
			break;
		}
		

	}

	@Override
	public void crearEquip() throws ExeptionNumeroInsuficientDeJugadors, IOException {
	
		String nomEquipNou = menu.menuNouEquip();
		Hashtable<Integer, Jugador> llista = new Hashtable<>();
		int opcio;
		do {
		menu.mostraMissatge("Quants jugadors vols afegir(minim15): ");
		opcio = menu.opcioUsuari();
		}while(opcio<15);
		
		for(int i = 0 ; i<opcio ; i++)
		{
		Jugador j1 = menu.demanarJugador();
		llista.put(j1.getDorsal(), j1);
		}
		
		Equip nouEquip = new Equip(nomEquipNou, llista);
		lligaCat.afegirEquip(nouEquip);
		
		//gestionarLliga();
		
	}

	@Override
	public void eliminarEquip() throws ExeptionNumeroInsuficientDeJugadors, IOException {
		
		Equip equipEliminar = menu.escollirEquip(lligaCat);
		lligaCat.eliminarEquip(equipEliminar);
		menu.mostraMissatge("Equip Eliminat!\n");
				//gestionarLliga();
		
	}
	
	private void afegeixJugador(Equip equip) throws ExeptionNumeroInsuficientDeJugadors, IOException {
		
		Jugador nouJugador;
		
		nouJugador=menu.demanarJugador();
		equip.afegirJugador(nouJugador);

		
		for (Jugador jugadors : equip.getLlistaJugadors().values()) {
			System.out.println(jugadors.getNom());
		}
		
		modificarEquip();
	}

	public void enrere() throws ExeptionNumeroInsuficientDeJugadors, IOException {
		ControladorMenu menuLliga = new ControladorMenu();
		menuLliga.iniciarMenu();
	}
	
	public void guardar() throws IOException {
		lligaCat.eliminarLliga();
		
		lligaCat.gurardaLiga();
			
	}
	
	public void canviaNomEquip() {
		Equip equipNomNou = menu.escollirEquip(lligaCat);
		menu.mostraMissatge("Escriu el nou nom del equip: ");
		String nom = menu.menuNouEquip();
		equipNomNou.setNomEquip(nom);
	}
	
	public void cargaSerializable() throws FileNotFoundException, IOException {
		
		ObjectOutputStream oos = new ObjectOutputStream( new FileOutputStream("ligaSerializable"));
		oos.writeObject(lligaCat);
		oos.close();
		
		
	}
	
	public void cargaJson() throws IOException {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String jsonOutput = gson.toJson(lligaCat);

		PrintWriter prJson = new PrintWriter(new FileWriter("lligaJson.json"));
		prJson.println(jsonOutput);
		
		prJson.close();
		
	}

}
