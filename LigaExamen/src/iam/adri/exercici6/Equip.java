package iam.adri.exercici6;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Equip implements Comparable<Equip>,Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String nomEquip;
	private int puntsLliga;
	private Hashtable<Integer, Jugador> llistaJugadors;
	public static int comptadorEquips;
	private File file;
	
	public Equip(String nomEquip, Hashtable<Integer, Jugador> llistaJugadors) {
		
		this.nomEquip = nomEquip;
		this.setLlistaJugadors(llistaJugadors);
		puntsLliga = 0;
		comptadorEquips++;
	} 
	
	public Equip(File file) throws ExeptionNumeroInsuficientDeJugadors,IOException {
		//this.setFile(file);
		this.puntsLliga=0;
		
		//Extraer el nombre del equipo desde la ruta file que se pasa en el contructor.
		nomEquip= file.getName();
		int posExt = nomEquip.indexOf(".txt");
		nomEquip=nomEquip.substring(0, posExt );
		
		
		llistaJugadors = new Hashtable<Integer, Jugador>();
		this.file = file;
		BufferedReader br = new BufferedReader(new FileReader(file));
		String line;
	    while ((line = br.readLine()) != null) {
	       
	    	Jugador jug = new Jugador(line);
	    	llistaJugadors.put(jug.getDorsal(),jug);
	    	
	    }
		
		
		 br.close();
	   
		 if(llistaJugadors.size()<15)
		 {
			throw new ExeptionNumeroInsuficientDeJugadors("Numero de jugadors incuficients, minim 15  ERROR:"+getNom());
		 }
		
	       
	       
	       // products = addProduct(products, newProduct);
	    }
		
	
	
	
	public void eliminarJugador(int dorsal) {
		
		llistaJugadors.remove(dorsal);
		
	}
	
	public void afegirJugador(Jugador jugAdd) {
		llistaJugadors.put(jugAdd.getDorsal(), jugAdd);
	}
	
	
	
	
	public void mostraLlistaJugadors() {
		
		for (Jugador llista : llistaJugadors.values()) {
			System.out.println("Nom: "+llista.getNom()+" Dorsal: "+llista.getDorsal());
		}
	}
	public Jugador[] creaAliniacioRandom()
	{
	
		
		Collection jugadores =  llistaJugadors.values();
		List<Jugador> aliniacionlista = new ArrayList<Jugador>(jugadores);
		Collections.shuffle(aliniacionlista);
		
		Jugador[] alineacion = new Jugador[11];
		
		for (int i = 0; i < alineacion.length; i++) {
			
			alineacion[i] = aliniacionlista.get(i);
		}
		
		return alineacion;
		
		
	}

	public String getNom() {
		return nomEquip;
	}
	
	public String getNomEquip() {
		return nomEquip;
	}

	public void setNomEquip(String nomEquip) {
		this.nomEquip = nomEquip;
	}

	public int getPuntsLliga() {
		return puntsLliga;
	}

	public void setPuntsLliga(int puntsLliga) {
		this.puntsLliga = puntsLliga;
	}

	public static int getComptadorEquips() {
		return comptadorEquips;
	}

	public static void setComptadorEquips(int comptadorEquips) {
		Equip.comptadorEquips = comptadorEquips;
	}

	public int getPunts() {
		return puntsLliga;
	}
	
	public void incrmentaPunts(int puntsAincrementar) {
		
		 puntsLliga = puntsLliga + puntsAincrementar;
	}
	
	public static int numEquips() {
		return comptadorEquips;
	}


	@Override
	public int compareTo(Equip o) {
		if(puntsLliga>o.puntsLliga) {
			return 1;
		}
		if(puntsLliga<o.puntsLliga) {
			return -1;
		}
		return 0;
	}


	public Hashtable<Integer, Jugador> getLlistaJugadors() {
		return llistaJugadors;
	}


	public void setLlistaJugadors(Hashtable<Integer, Jugador> llistaJugadors) {
		this.llistaJugadors = llistaJugadors;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}
}

