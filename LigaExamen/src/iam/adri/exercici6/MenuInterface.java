package iam.adri.exercici6;

import java.io.IOException;
import java.util.Hashtable;

public interface MenuInterface {

	/**
	 * Mostrar� el men� pincipal
	 * 
	 * @return Retorna la opci� seleccionada per l�usuari
	 */
	int menuPrincipal();

	/**
	 * Demana a l�usuari les opcions per gestionar la lliga
	 * 
	 * @return la opci� seleccionada
	 */
	int menuGestionarLliga();

	/**
	 * Opcions per modicficar l�equip
	 * 
	 * @return la opci� seleccionada
	 */
	int menuModificarEquip();

	/**
	 * Demana a l�usuari el nom del equip
	 * 
	 * @return el nou nom del equip
	 */
	String menuNouEquip();

	/**
	 * Per demanar tots els jugadors d�un equip
	 * 
	 * @return una hashtable amb els jugadors
	 */
	Hashtable<Integer, Jugador> demanarJugadors();

	/**
	 * Escull un equip de la liga
	 * 
	 * @param lliga
	 *            on estan els equips a mostrar el seu nom
	 * @return el equip escollit
	 */
	Equip escollirEquip(Lliga lliga);

	/**
	 * Demana les dades del jugador a crear
	 * 
	 * @return el jugador creat
	 */
	Jugador demanarJugador();

	void eliminarJugador(Equip equip) throws ExeptionNumeroInsuficientDeJugadors, IOException;

	String demanarNomEquip();

	Equip demanarEquip();

}