package iam.adri.exercici6;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.Hashtable;
import java.util.Scanner;

public class Jugador implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String nom;
	private String cognom;
	private int	dorsal;
	private String posicio;
	private int edat;
	private String alsada;
	
	private String linia;
	
	public Jugador(String nom,String cognom, int dorsal, String posicio, int edat, String alsada) {
		this.nom = nom;
		this.cognom = cognom;
		this.dorsal = dorsal;
		this.posicio = posicio;
		this.edat = edat;
		this.alsada = alsada;
		
	}

	public Jugador(String linia) {
		this.linia = linia;
		
		String[] Liniadesglosada;
		Liniadesglosada=linia.split("#");
		
		nom=Liniadesglosada[0];
		cognom=Liniadesglosada[1];
		dorsal=Integer.parseInt(Liniadesglosada[2]);
		posicio=Liniadesglosada[3];
		edat=Integer.parseInt(Liniadesglosada[4]);
		alsada=Liniadesglosada[5];		
		
	}
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getCognom() {
		return cognom;
	}

	public void setCognom(String cognom) {
		this.cognom = cognom;
	}

	public int getDorsal() {
		return dorsal;
	}

	public void setDorsal(int dorsal) {
		this.dorsal = dorsal;
	}

	public String getPosicio() {
		return posicio;
	}

	public void setPosicio(String posicio) {
		this.posicio = posicio;
	}

	public int getEdat() {
		return edat;
	}

	public void setEdat(int edat) {
		this.edat = edat;
	}

	public String getAlsada() {
		return alsada;
	}

	public void setAlsada(String alsada) {
		this.alsada = alsada;
	}



	
	public String toString() {
		return "   NOM: "+nom + "  \tCOGNOM:" + cognom + "  \tDorsal" + dorsal + "  \tPosicio:"+ posicio+"]\n";
	}
	
	public String toStringModificat() {
	return nom+"#"+cognom+"#"+dorsal+"#"+posicio+"#"+edat+"#"+alsada;
	}

	

}
