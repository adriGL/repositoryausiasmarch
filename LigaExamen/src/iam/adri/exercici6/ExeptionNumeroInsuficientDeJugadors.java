package iam.adri.exercici6;

public class ExeptionNumeroInsuficientDeJugadors extends Exception {
	
	public ExeptionNumeroInsuficientDeJugadors() {
		super();
	}
	
	public ExeptionNumeroInsuficientDeJugadors(String msg) {
		super(msg);
	}

}
