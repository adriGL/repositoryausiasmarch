package iam.adri.exercici6;

import java.io.IOException;

public interface ControladorInterface {
	void iniciarMenu() throws IOException, ExeptionNumeroInsuficientDeJugadors;

	void jugarLliga() throws IOException;

	void gestionarLliga() throws ExeptionNumeroInsuficientDeJugadors, IOException;

	void modificarEquip() throws ExeptionNumeroInsuficientDeJugadors, IOException;

	void crearEquip() throws ExeptionNumeroInsuficientDeJugadors, IOException;

	void eliminarEquip() throws ExeptionNumeroInsuficientDeJugadors, IOException;
}
