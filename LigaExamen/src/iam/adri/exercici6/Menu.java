package iam.adri.exercici6;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;
import java.util.Scanner;

public class Menu implements MenuInterface ,Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static Scanner input = new Scanner(System.in);
	Lliga lliga;
	public Menu(Lliga lliga) throws ExeptionNumeroInsuficientDeJugadors, IOException {
		this.lliga = lliga;
	}

	 public int menuCarrega() {
		System.out.println("1.Carrega desde file txt");
		System.out.println("2.Carrega desde file Serialitzat");
		System.out.println("3.Carrega desde file JSON");
		System.out.println("4.Sortir");
		int opcio=Integer.parseInt(input.nextLine());
		
	return  opcio;
	}
	@Override
	public int menuPrincipal() {
	
		
		System.out.println("1.Jugar Lliga");
		System.out.println("2.Gestionar Lliga");
		System.out.println("3.Exportar");
		System.out.println("4.Sortir");
		int opcio=Integer.parseInt(input.nextLine());
		
	return  opcio;
	}

	public int menuExporta() {
		System.out.println("1.Exportar lliga Serialitzada ");
		System.out.println("2.Exportar lliga JSON ");
		System.out.println("3.Enrere");
		int opcio = Integer.parseInt(input.nextLine());
		
		return opcio;
	}
	@Override
	public int menuGestionarLliga() {
	
		System.out.println("1.Crea Equip");
		System.out.println("2.Esborra Equip");
		System.out.println("3.Modifica Equip");
		System.out.println("4.Enrere");
		System.out.println("5.Guardar");
		
		int opcio=Integer.parseInt(input.nextLine());
		
		
		return  opcio ;
	}
	
	
	@Override
	public int menuModificarEquip() {
		
		System.out.println("1.Elimina Jugador");
		System.out.println("2.Afegeix Jugador");
		System.out.println("3.Canvia nom equip");
		System.out.println("4.Enrere");
		int opcio=Integer.parseInt(input.nextLine());
	
		
		return  opcio ;
	}

	@Override
	public String menuNouEquip() {
		
		
		System.out.println("-Indica el nom:");
				
		String opcio=input.nextLine();
		
		
		return  opcio ;
	
	}
	public void mostraMissatge(String misatge) {
		System.out.println(misatge);
	}
	public int opcioUsuari() {
		
		int opcio =Integer.parseInt(input.nextLine());
		
		return opcio;
	}

	@Override
	public Hashtable<Integer, Jugador> demanarJugadors() {
		/**
		 * Per demanar tots els jugadors d�un equip
		 * 
		 * @return una hashtable amb els jugadors
		 */
		Hashtable<Integer, Jugador> jugadores=null;
		String nomEquip = demanarNomEquip();
		
		
		for(int i = 0 ; i<lliga.arrayEquips.size(); i++) {
			if(lliga.arrayEquips.get(i).getNomEquip().equals(nomEquip)) {
				
				jugadores=lliga.arrayEquips.get(1).getLlistaJugadors();
				return jugadores;
			}
		}
		
		
		
		return jugadores;
	}
	
	@Override	
	public String demanarNomEquip() {
		mostraMissatge("Equips: ");
		lliga.mostraEquips();
		
		System.out.println("Escull un Equip");
		String equipEscollit=input.nextLine();
		
		return equipEscollit;
	}
	
	
	@Override
	public Equip escollirEquip(Lliga lliga) {
		
		Equip equip1 = null;
		String equipEscollit = demanarNomEquip();
		
		for(Equip equips:lliga.arrayEquips)
		{
			if(equips.getNom().equals(equipEscollit))
			{
				equip1= equips;
			}
						
		}		
		return equip1;
	}

	@Override
	public Jugador demanarJugador() {
		
		System.out.println("Escriu el nom");
		String nom=input.nextLine();
		System.out.println("Escriu el Cognom");
		String cognom=input.nextLine();
		System.out.println("Escriu el Dorsal");
		int dorsal=Integer.parseInt(input.nextLine());
		System.out.println("Escriu el Posicio");
		String posicio=input.nextLine();
		System.out.println("Escriu el Edat");
		int edat=Integer.parseInt(input.nextLine());
		System.out.println("Escriu el Alsada(1.02)");
		String alsada=input.nextLine();
		
		Jugador nouJugador = new Jugador(nom,cognom,dorsal,posicio,edat,alsada);
		
		return nouJugador;
	}

	@Override
	public void eliminarJugador(Equip equip) {
		int jugEliminar;
		
		mostraMissatge("Escull el dorsal del Judador que vols eliminar :");
		
		equip.mostraLlistaJugadors();
		jugEliminar = Integer.parseInt(input.nextLine());
		equip.eliminarJugador(jugEliminar);
		
		mostraMissatge("Jugador eliminat Correctament\n");
		
		
	}



	@Override
	public Equip demanarEquip() {
		
		return null;

	}
}
