package iam.adri.exercici6;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class Partit implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//TODO atribute
	
	Equip equipLocal;
	Equip equipVisitant;
	private int golsEquipLocal;
	private int golsEquipVisitant;
	private List<Jugador> goleadoresV;
	private List<Jugador> goleadoresL;
	private Jugador[] alineacionLocal;
	private Jugador[] alienacionVisitante;
	static List<Jugador> goleadoresLiga = new ArrayList<Jugador>();
	
	//TODO Constructor.
	
	public Partit(Equip equipLocal, Equip equipVisitant) {
		this.equipLocal =  equipLocal;
		this.equipVisitant = equipVisitant;
		golsEquipLocal = 0;
		golsEquipVisitant = 0;        
		
		alienacionVisitante = equipVisitant.creaAliniacioRandom();
		alineacionLocal = equipLocal.creaAliniacioRandom();
	}
	
	
	
	
	// TODO method
	
	public void marcaEquipLocal(int golsLocal) {
		golsEquipLocal = golsLocal;
	}


	public void marcaEquipVisitant(int golsVisitant) {
		golsEquipVisitant = golsVisitant;
		
	}
	
	public void marcaJudadorLocal() {
		
		goleadoresL = new ArrayList<Jugador>();
		
				
		for(int i = 0; i<golsEquipLocal; i++) {
			if(golsEquipLocal>0)
			{
			Random r = new Random();
			int num = r.nextInt(10);
			goleadoresL.add(alineacionLocal[num]);
			//alineacionLocal[num].setGols(alineacionLocal[num].getGols()+1);
			goleadoresLiga.add(alineacionLocal[num]);
			
			}
		}
				
	}
	
	public void marcaJudadorVisitant() {

		goleadoresV = new ArrayList<Jugador>();
		
				
		for(int i = 0; i<golsEquipVisitant; i++) {
			if(golsEquipVisitant>0)
			{
			Random r = new Random();
			int num = r.nextInt(10);
			goleadoresV.add(alienacionVisitante[num]);
			goleadoresLiga.add(alineacionLocal[num]);
			}
		}
		
	}
	
	public String fi()
	{
		if(golsEquipLocal>golsEquipVisitant) {
			equipLocal.incrmentaPunts(3);
			return "Guanyador: "+ " " + equipLocal.getNom();
			
		}
		
		else if (golsEquipLocal<golsEquipVisitant) {
			equipVisitant.incrmentaPunts(3);
			return "Guanyador: "+equipVisitant.getNom();
		}
		
		else { 
			equipLocal.incrmentaPunts(1);
			equipVisitant.incrmentaPunts(1);
				return"EMPAT";
		}
		
	
	}
	
	
	public Equip getEquipLocal() {
		return equipLocal;
	}


	public void setEquipLocal(Equip equipLocal) {
		this.equipLocal = equipLocal;
	}


	public Equip getEquipVisitant() {
		return equipVisitant;
	}


	public void setEquipVisitant(Equip equipVisitant) {
		this.equipVisitant = equipVisitant;
	}


	public String toString()
	{
		return "\n"+fi()+"\nEl Resultat del partit: "+equipLocal.getNom()+" "+golsEquipLocal+" - "+equipVisitant.getNom()+" "+golsEquipVisitant;

	}

	
	



	public List<Jugador> getGoleadoresV() {
		return goleadoresV;
	}




	public void setGoleadoresV(List<Jugador> goleadoresV) {
		this.goleadoresV = goleadoresV;
	}




	public List<Jugador> getGoleadoresL() {
		return goleadoresL;
	}




	public void setGoleadoresL(List<Jugador> goleadoresL) {
		this.goleadoresL = goleadoresL;
	}




	public Jugador[] getAlineacionLocal() {
		return alineacionLocal;
	}




	public void setAlineacionLocal(Jugador[] alineacionLocal) {
		this.alineacionLocal = alineacionLocal;
	}




	public Jugador[] getAlienacionVisitante() {
		return alienacionVisitante;
	}




	public void setAlienacionVisitante(Jugador[] alienacionVisitante) {
		this.alienacionVisitante = alienacionVisitante;
	}




	public int getGolsEquipLocal() {
		return golsEquipLocal;
	}

	public void setGolsEquipLocal(int golsEquipLocal) {
		this.golsEquipLocal = golsEquipLocal;
	}

	public int getGolsEquipVisitant() {
		return golsEquipVisitant;
	}

	public void setGolsEquipVisitant(int golsEquipVisitant) {
		this.golsEquipVisitant = golsEquipVisitant;
	}




	
}

