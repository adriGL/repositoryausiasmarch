package iam.adri.exercici6;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

public class Lliga implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	ArrayList<Equip> arrayEquips;
	ArrayList<Partit> arrayPartits;
	File rutaLliga;
	

	public Lliga(ArrayList<Equip> arrayEquips) {
		// TODO declarem una variable que cont� l'espai de l'array de partits(nummero
		// d'equips * nummero d'equips -nummero d'equips)
		this.arrayEquips = arrayEquips;

		// int espaiArrayEquip = arrayEquips.size() *
		// arrayEquips.size()-arrayEquips.size();
		// int k=0;
		arrayPartits = new ArrayList<Partit>();

		for (int i = 0; i < arrayEquips.size(); i++) {
			for (int j = 0; j < arrayEquips.size(); j++) {
				if (i != j) {

					arrayPartits.add(new Partit(arrayEquips.get(i), arrayEquips.get(j)));

					// arrayPartits[k++] = new Partit(arrayEquips[i],arrayEquips[j]);
				}

			}
		}

	}

	public Lliga(File rutaLliga) throws ExeptionNumeroInsuficientDeJugadors, IOException {

		this.rutaLliga = rutaLliga;
		arrayEquips= new ArrayList<Equip>();
		arrayPartits = new ArrayList<Partit>();

		String files;
		File folder = new File(rutaLliga.toString());
		File[] llistafitxers = folder.listFiles();
		File filehist = new File (folder+"/HistoricPartits.txt");
		String f1h = filehist.toString();

		if (rutaLliga.isDirectory()) {

			for (int i = 0; i < llistafitxers.length; i++) {
				
				if (llistafitxers[i].isFile()) {
									
				if(llistafitxers[i].toString().equals(filehist.toString())==false) {
					files = llistafitxers[i].getPath();
					File f1 = new File(files);
					
					arrayEquips.add(new Equip(f1));
				 }
				}

			}
			
			for (int i = 0; i < arrayEquips.size(); i++) {
				for (int j = 0; j < arrayEquips.size(); j++) {
					if (i != j) {

						arrayPartits.add(new Partit(arrayEquips.get(i), arrayEquips.get(j)));

						// arrayPartits[k++] = new Partit(arrayEquips[i],arrayEquips[j]);
					}

				}
			}
			
		} else
			System.out.println("No es un directori.");

	}

	public void jugarPartit(int numeroPartit) {

		int numeroAleatori = ThreadLocalRandom.current().nextInt(0, 5 + 1);
		arrayPartits.get(numeroPartit).marcaEquipLocal(numeroAleatori);

		int numeroAleatori2 = ThreadLocalRandom.current().nextInt(0, 5 + 1);
		arrayPartits.get(numeroPartit).marcaEquipVisitant(numeroAleatori2);

		arrayPartits.get(numeroPartit).fi();
	}

	public void registraLliga() throws IOException {
		
		String file =rutaLliga+"./HistoricPartits.txt";
		PrintWriter fw = new PrintWriter(new FileWriter(file, true));
		
		for (int j = 0; j < arrayPartits.size(); j++) {

		//Escric en Historial el resultat del partit.
		
		fw.println("Partit " + j);
		fw.println("Local: " + arrayPartits.get(j).getEquipLocal().getNom() + " Visitant: "
				+ arrayPartits.get(j).getEquipVisitant().getNom());
		fw.println();
		fw.println("Alineacio Local");
		fw.println(Arrays.toString(arrayPartits.get(j).getAlineacionLocal()));
		fw.println("Alineacio Visitant");
		fw.println(Arrays.toString(arrayPartits.get(j).getAlienacionVisitante()));
		
		//RESULTAT	
		fw.println(arrayPartits.get(j).toString());
		fw.println();
		
		//Golejadors:
		
		if(arrayPartits.get(j).getGolsEquipLocal()>0) {
			fw.println("GOLS LOCAL");
				fw.println(golsMarcatsPerPartit(arrayPartits.get(j).getGoleadoresL()));
		}
		if(arrayPartits.get(j).getGolsEquipVisitant()>0) {
			fw.println("\nGOLS VISITANT");
				fw.println(golsMarcatsPerPartit(arrayPartits.get(j).getGoleadoresV()));
		}
		fw.println();
		
		fw.println("\n\nCLASIFICACIO");
		fw.println(this.classificacio()+"\n\n");
		
	}
		fw.println("El Golejador de la lliga es "+mostraMaximGolejador());
		fw.close();
	}
		
	
	public void jugaLliga() throws IOException {

		for (int j = 0; j < arrayPartits.size(); j++) {
			
			//La aliniacion se crea cuando se crea un partido
			
			
			//Gols aleatoris
			int numeroAleatori = ThreadLocalRandom.current().nextInt(0, 5 + 1);
			arrayPartits.get(j).marcaEquipLocal(numeroAleatori);
			arrayPartits.get(j).marcaJudadorLocal();
			int numeroAleatori2 = ThreadLocalRandom.current().nextInt(0, 5 + 1);
			arrayPartits.get(j).marcaEquipVisitant(numeroAleatori2);
			arrayPartits.get(j).marcaJudadorVisitant();
		}

		
		registraLliga();
	}
	
	public void afegirEquip(Equip equip) {
		arrayEquips.add(equip);
	}
	public void eliminarEquip(Equip equip) {
		arrayEquips.remove(equip);
	}
	public String classificacio() {
		String resultat = "";

		Collections.sort(arrayEquips);

		for (int i = 0; i < arrayEquips.size(); i++) {

			resultat = resultat + arrayEquips.get(i).getNom() + arrayEquips.get(i).getPunts() + "\n";
		}

		return resultat;

	}
	
	public String golsMarcatsPerPartit(List<Jugador> llista) {
		String quantitat = " ";
		
		
		Set<Jugador> quipu = new HashSet<Jugador>(llista);
		
        for (Jugador key : quipu) {
             quantitat = quantitat+"\n"+ key.getNom()+" "+ key.getCognom() + " " + Collections.frequency(llista, key);
        }
		
        
        
		return quantitat;
	}
	
	public String mostraMaximGolejador() {
		
		String maximGolejador="";
		int cont = 0;
		
		for (Jugador pixixi : Partit.goleadoresLiga) {
			 if(cont<Collections.frequency(Partit.goleadoresLiga, pixixi))
				 {
				 	cont = Collections.frequency(Partit.goleadoresLiga, pixixi);
				 	maximGolejador=pixixi.getNom();
				 }
			
		}
		
		
		
		return maximGolejador+" amb "+cont+" gols!!!";
	}

	public void mostraEquips() {

		for (Equip equip : arrayEquips) {
			System.out.println(equip.getNom());
		}
	}
	
	public void eliminarLliga() {
		
		for (File files :rutaLliga.listFiles()) {
			files.delete();
		}
		
	}

	public void gurardaLiga() throws IOException {
	
		for (int i = 0; i < arrayEquips.size(); i++) {
			PrintWriter fw = new PrintWriter(new FileWriter("./LligaCat/"+arrayEquips.get(i).getNomEquip()+".txt"));
			Enumeration<Jugador> llistaModificada = arrayEquips.get(i).getLlistaJugadors().elements();
		
			while(llistaModificada.hasMoreElements()) {
			
				fw.println(llistaModificada.nextElement().toStringModificat());
			}
			
			fw.close();	
		}
		
	}
}
