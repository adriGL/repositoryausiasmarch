package org.hotel.bcn;

public class Hotel {

	String nom;
	int places;
	
	public Hotel(String nom, int places) {
		
		this.nom=nom;
		this.places=places;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getPlaces() {
		return places;
	}

	public void setPlaces(int places) {
		this.places = places;
	}
	
	
}
